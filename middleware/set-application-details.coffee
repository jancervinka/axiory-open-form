express = require 'express'
makeRequest = require 'request'
url = require 'url'
steps = require('config').get 'steps'
renderError = require('../utils').renderError

module.exports = (req, res, next) ->
  isId = (id) ->
    return if id.match(/^[a-z0-9]{30,40}$/) then true else false

  req.step = 'intro'
  req.partnerCode = if req.query?.ib then req.query?.ib else req.cookies?.ib

  if not req.params.id then return next()
  if not isId req.params.id then return res.sendStatus(404)

  makeRequest {url: "#{req.brand.apiUrl}/account-applications/#{req.params.id}", gzip: true}, (err, response, body) ->
    renderedError = renderError(err, response, body, req, res)
    if renderedError then return

    data = if body then JSON.parse(body) else next()

    if data.ibCode then req.partnerCode = data.ibCode

    currentStepIndex = steps.indexOf(data.step)
    req.step = if currentStepIndex >= steps.length-1 then currentStepIndex else steps[currentStepIndex+1]
    next()


