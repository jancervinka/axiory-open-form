express = require 'express'
makeRequest = require 'request'
config = require 'config'
renderError = require('../utils').renderError
#logger = require './logger'

brandExtractor = (req, res, next) ->
	hostname = unless req.hostname is 'localhost' then req.hostname else config.get 'defaultHostname'

	makeRequest {url: "#{config.get 'apiUrls.default'}/brands", gzip: true, qs: {domain: hostname}}, (err, response, body) ->
		renderedError = renderError(err, response, body, req, res)
		if renderedError then return
		if not body
			logger.error "No brand found for #{req.hostname}"
			return res.render 'serverError',
			  brand: req.brand

		req.brand = JSON.parse(body)
		req.brand.apiUrl = config.get "apiUrls.#{req.brand.api}"
		if req.query.plain is 'true' then req.brand.id = "#{req.brand.id}-plain"
		next()

module.exports = brandExtractor
