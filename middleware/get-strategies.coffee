express = require 'express'
makeRequest = require 'request'
renderError = require('../utils').renderError
logger = require '../logger'

module.exports = (req, res, next) ->
  if not req.partnerCode then return next()
  if req.step isnt 'trading-account-details' then return next()

  makeRequest {url: "#{req.brand.apiUrl}/partners/#{req.partnerCode}/strategies", gzip: true}, (err, response, body) ->
    if err then logger.error 'Error while getting strategies from API'
    if response.statusCode isnt 200 then logger.error "API returned #{response.statusCode} while getting strategies for partner #{req.partnerCode}"

    strategies = if body then JSON.parse(body) else {}
    req.strategies = (strategy.name for strategy in strategies)
    next()
