Code = require 'code'
expect = Code.expect

browser = require '../components/browser'
i18n = require '../components/i18n'

describe 'Trading Account Details', ->
  it 'should be at trading account details', ->
    expect(browser.location.pathname.substring 1).to.equal process.env.APPLICATION_ID
    browser.assert.textExists 'h1', i18n.t('steps.trading-account-details.title')

  it 'should accept submission', (done) ->
    browser
      .select 'leverage', i18n.t('fields.leverage.values.100')
      .fill('mt4Password', 'Abcde1234')
      .fill('mt4PasswordRepeat', 'Abcde1234')
      .select 'currency', 'EUR'
      .wait ->
        browser.pressButton 'input[type=submit]'
          .then (error) ->
            expect(error).to.be.undefined()
            browser.assert.success()
            browser.assert.redirected()
            return
          .then done, done
