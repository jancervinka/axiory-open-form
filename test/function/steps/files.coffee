Code = require 'code'
expect = Code.expect

browser = require '../components/browser'
i18n = require '../components/i18n'

describe 'Files', ->
  it 'should be at files', ->
    expect(browser.location.pathname.substring 1).to.equal process.env.APPLICATION_ID 
    browser.assert.textExists 'h1', i18n.t('steps.files.title')

  it 'should submit without files', (done) ->
    browser.choose i18n.t('fields.addFiles.values.later')
    .wait ->
        browser.pressButton 'input[type=submit]'
          .then (error) ->
            expect(error).to.be.undefined()
            browser.assert.success()
            browser.assert.redirected()
            return
          .then done, done