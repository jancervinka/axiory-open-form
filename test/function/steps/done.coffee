Code = require 'code'
expect = Code.expect

browser = require '../components/browser'
i18n = require '../components/i18n'

describe 'Done', ->
  it 'should be shown', ->
    expect(browser.location.pathname.substring 1).to.equal process.env.APPLICATION_ID
    browser.assert.textExists 'h1', i18n.t('steps.done.title')
