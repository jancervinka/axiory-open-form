Code = require 'code'
expect = Code.expect

browser = require '../components/browser'
i18n = require '../components/i18n'

describe 'Initial Page', ->
  it 'should load successfully', ->
    browser.assert.success()

  it 'should show agreements', ->
    browser.assert.link 'a.agreement', i18n.t('steps.intro.agreementsSection.orderExecutionPolicy'), 'http://www.axiory.eu/documents#oep'
    browser.assert.link 'a.agreement', i18n.t('steps.intro.agreementsSection.riskDisclosure'), 'http://www.axiory.eu/documents#rw'
    browser.assert.link 'a.agreement', i18n.t('steps.intro.agreementsSection.privacyPolicy'), 'http://www.axiory.eu/documents#pp'
    browser.assert.link 'a.agreement', i18n.t('steps.intro.agreementsSection.termsAndConditions'), 'http://www.axiory.eu/terms-and-conditions'

  it 'should show contact form', ->
    browser.assert.textExists 'h2', i18n.t('steps.intro.contactInformationSection.title')

  it 'should refuse empty submissions', (done) ->
    browser
      .pressButton 'input[type=submit]'
      .then (error) ->
        expect(error).to.be.undefined()
        browser.assert.url({pathname: '/'})
        return
      .then done, done

  it 'should accept complete initial submission', (done) ->
    browser
      .fill('fullName', 'Jan Červinka s.r.o')
      .fill('email', 'cervinkah@gmail.com')
      .fill('phone', '+123456789')
      .check('iAgree')
      .pressButton 'input[type=submit]'
      .then (error) ->
        expect(error).to.be.undefined()
        browser.assert.success()
        browser.assert.redirected()
        return
      .then done, done
