Code = require 'code'
expect = Code.expect

browser = require '../components/browser'
i18n = require '../components/i18n'

describe 'Personal Details', ->
  it 'should be at personal details', (done) ->
      process.env.APPLICATION_ID = browser.location.pathname.substring 1
      expect(process.env.APPLICATION_ID).to.match /^[a-z0-9]{30,40}$/
      browser.assert.textExists 'h1', i18n.t('steps.personal-details.title')
      done()

  it 'should autocomplete nationality', (done) ->
    browser.fillInAutocomplete '#nationality', 'Cze', '#ui-id-1', (browser) ->
      browser.assert.input('select[name=nationality]', "Czech Republic")
      done()
  
  it 'should autocomplete country', (done) ->
    browser.fillInAutocomplete '#country', 'USA', '#ui-id-2', (browser) ->
      browser.assert.input('select[name=country]', "United States")
      done()

  it 'should accept personal details submission', (done) ->
    browser
      .choose i18n.t('fields.gender.values.male')
      .fill 'dayOfBirth', '24'
      .select 'monthOfBirth', i18n.t('fields.monthOfBirth.values.1')
      .fill 'yearOfBirth', '1989'
      .fill 'cityOfBirth', 'City'
      .fill 'idNumber', '123'
      .fill 'street', 'Ulice 456'
      .fill 'city', 'Město'
      .fill 'postalCode', '123 456'
      .choose i18n.t('fields.politicalExposure.values.no')
      .choose i18n.t('fields.usReportable.values.no')
      .wait ->
        browser.pressButton 'input[type=submit]'
          .then (error) ->
            expect(error).to.be.undefined()
            browser.assert.success()
            browser.assert.redirected()
            return
          .then done, done
