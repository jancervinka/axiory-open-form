Code = require 'code'
expect = Code.expect

browser = require '../components/browser'
i18n = require '../components/i18n'

describe 'Investment Questionaire', ->
  it 'should be at investment questionaire', ->
    expect(browser.location.pathname.substring 1).to.equal process.env.APPLICATION_ID 
    browser.assert.textExists 'h1', i18n.t('steps.investment-questionaire.title')

  it 'should accept investment questionaire submission', (done) ->
    browser
      .select 'education', i18n.t('fields.education.values.none')
      .select 'employmentStatus', i18n.t('fields.employmentStatus.values.student')
      .select 'annualIncome', i18n.t('fields.annualIncome.values.under-50k-eur')
      .select 'netWorth', i18n.t('fields.netWorth.values.under-50k-eur')
      .choose i18n.t('fields.financialSectorEmployment.values.no')
      .choose i18n.t('fields.portfolioSize.values.under-500k-eur')
      .select 'generalInvestmentObjective', i18n.t('fields.generalInvestmentObjective.values.capital-maintenance')
      .select 'investingExperience', i18n.t('fields.investingExperience.values.none')
      .choose i18n.t('fields.tradingExperience.values.no')
      .select 'investmentPourpose', i18n.t('fields.investmentPourpose.values.other')
      .select 'investmentPeriod', i18n.t('fields.investmentPeriod.values.0-6-months')
      .select 'investmentAmount', i18n.t('fields.investmentAmount.values.under-10k-eur')
      .select 'fundsOrigin', i18n.t('fields.fundsOrigin.values.savings')
      .select 'acceptedRisk', i18n.t('fields.acceptedRisk.values.low')
      .wait ->
        browser.pressButton 'input[type=submit]'
          .then (error) ->
            expect(error).to.be.undefined()
            browser.assert.success()
            browser.assert.redirected()
            return
          .then done, done