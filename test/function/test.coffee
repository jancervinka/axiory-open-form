process.env.NODE_ENV = 'test'

Code = require 'code'
expect = Code.expect

app = require '../../server'
server = app.listen 3003

browser = require './components/browser'

describe 'Account Application', ->

  before (done) ->
    browser.visit '/?ib=1234', done

  afterEach (done) ->
    browser.wait ->
      done()

  require './steps/initial'

  require './steps/personal-details'

  require './steps/investment-questionaire'

  require './steps/files'

  require './steps/trading-account-details'

  require './steps/done'

  after (done) ->
    server.close(done)
