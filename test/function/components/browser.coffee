Code = require 'code'
expect = Code.expect

Browser = require 'zombie'
Browser.localhost 'start.axiory.eu', 3003

# Browser.debug()

Browser.Assert.prototype.textExists = (selector, text) ->
  texts = (e.textContent for e in browser.queryAll selector)
  expect(texts, "Array contains: #{texts}").to.include(text)

Browser.extend (browser) ->

  browser.fillInAutocomplete = (selector, value, list, done) ->

    listIsPopulated = (window) ->
      return window.document.querySelector "#{list} li"

    browser.wait ->
      browser.evaluate "$('#{selector}').siblings('.ui-autocomplete-input').focus().val('#{value}').keydown()"
      browser.wait listIsPopulated, ->
        browser.evaluate "$('#{list} li').first().trigger('mouseenter').click()"
        done(browser)

browser = Browser.create()

module.exports = browser
