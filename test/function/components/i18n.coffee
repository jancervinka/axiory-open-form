i18n = require 'i18next'
i18n.init {resGetPath: '../../locales/__lng__/__ns__.json'}
i18n.setLng 'en'

module.exports = i18n