require('coffee-script/register');
var app = require('./server');
var logger = require ('./logger');

app.listen(process.env.PORT || 4000, function(){
  logger.info('Open Form server started');
});
