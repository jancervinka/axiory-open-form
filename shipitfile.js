module.exports = function (shipit) {
  require('shipit-deploy')(shipit);

  var fs = require('fs');

  var config = {
    default: {
      servers: 'webapp@178.62.8.82',
      repositoryUrl: 'https://jancervinka@bitbucket.org/jancervinka/axiory-open-form',
      ignores: ['.git', 'coffeescripts', 'sass', 'test', 'shipitfile.js'],
      keepReleases: 3,
      key: '/Users/jancervinka/.ssh/id_rsa',
    },
    production: {
      webappPath: '/webapps/start.axiory.eu',
      workspace: '/tmp/start.axiory.eu',
      deployTo: '/opt/web/start.axiory.eu',
      branch: 'master',
      passengerfile: {
        ssl: true,
        ssl_port: 443,
        ssl_certificate: "/opt/web/ssl/start.axiory.crt",
        ssl_certificate_key: "/opt/web/ssl/start.axiory.key",
        environment: "production",
        "server_names": ["start.axiory.eu", "start.axiory.cz", "start.axiory.com", "start.axiory.asia", "open.pivot-markets.com"],
        rolling_restarts: "on",
        min_instances: 2
      }
    },
    staging: {
      webappPath: '/webapps/start-staging.axiory.eu',
      workspace: '/tmp/start-staging.axiory.eu',
      deployTo: '/opt/web/start-staging.axiory.eu',
      branch: 'staging',
      passengerfile: {
        environment: "staging",
        "server_names": ["start-staging.axiory.eu", "start-staging.axiory.cz", "start-staging.axiory.com", "start-staging.axiory.asia",  "open-staging.pivot-markets.com"],
        rolling_restarts: "on",
        min_instances: 0
      }
    }
  };

  shipit.initConfig(config);

  shipit.blTask('createPassengerfile', function(){
    return fs.writeFile(shipit.config.workspace+'/Passengerfile.json', JSON.stringify(shipit.config.passengerfile, null, 4), function(err) {
      if(err) {
        shipit.log(err);
      } else {
        shipit.emit('passengerfileCreated');
        shipit.log('Passengerfile created');
      }
    });
  });

  // wtith Passenger 5 RC1 this can be removed and I can deploy directly to /webapps and instruct passenger to start from current folder
  shipit.blTask('linkToWebapps', function(){
    return shipit.remote('ln -nfs '+shipit.releasePath+' '+shipit.config.webappPath)
    .then(function(){
      shipit.emit('linked');
    })
    .then(function () {
      shipit.log('Linked');
    });
  });

  shipit.blTask('restart', function(){
    return shipit.remote('passenger-config restart-app '+shipit.config.webappPath+' --ignore-app-not-running --rolling-restart')
    .then(function(){
      shipit.emit('restarted');
    })
    .then(function () {
      shipit.log('Restarted');
    });
  });

  shipit.blTask('install', function(){
    return shipit.local('npm install', {cwd: shipit.config.workspace})
    .then(function(){
      shipit.emit('installed');
    })
    .then(function () {
      shipit.log('Installed');
    });
  });

  shipit.blTask('build', function(){
    return shipit.local('gulp productionBuild', {cwd: shipit.config.workspace})
    .then(function(){
      shipit.emit('built');
    })
    .then(function () {
      shipit.log('Built');
    });
  });

  shipit.on('built', function(){
    return shipit.start('createPassengerfile');
  });

  shipit.on('published', function(){
    return shipit.start('linkToWebapps');
  });

  shipit.on('fetched', function(){
    return shipit.start('install');
  });

  shipit.on('installed', function(){
    return shipit.start('build');
  });

  shipit.on('linked', function(){
    return shipit.start('restart');
  });

};
