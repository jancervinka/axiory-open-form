logger = require './logger'

module.exports =
  renderError: (err, response, body, req, res) ->
    if (err)
      logger.error "Error returned from API - #{err}"
      if (not req.brand)
        logger.error "No brand information!"
        res.render 'noBrandError'
        return true
      res.render 'serverError'
      return true

    switch response.statusCode
      when 200, 201
        return false
      when 400
        if (not req.brand)
          logger.error "No brand information!"
          res.render 'noBrandError'
          return true

        message = JSON.parse(body).message
        if message is 'countryNotAllowed'
          logger.warn "Country not allowed"
          res.render 'notAllowed',
            brand: req.brand
            message: message
        else
          logger.error "Error 400 returned from API"
          res.render 'serverError',
            brand: req.brand
      when 404
        logger.error "Error 404 returned from API"
        if (not req.brand)
          logger.error "No brand information!"
          res.render 'noBrandError'
          return true
        res.render 'notFound',
          brand: req.brand
      else
        logger.error "Server error returned from API"
        if (not req.brand)
          logger.error "No brand information!"
          res.render 'noBrandError'
          return true
        res.render 'serverError',
          brand: req.brand

    return true
