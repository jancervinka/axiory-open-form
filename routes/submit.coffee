express = require 'express'
router = express.Router()
Request = require 'request'
steps = require('config').get 'steps'
renderError = require('../utils').renderError
logger = require '../logger'
multiparty = require 'multiparty'
fs = require 'fs'
logger = require './../logger'

handleResponse = (err, response, body, req, res) ->

  renderedError = renderError(err, response, body, req, res)
  if renderedError then return

  payload = JSON.parse(body)
  id = encodeURIComponent(payload.id)
  url = if req.query.plain is 'true' then "/#{id}?plain=true" else "/#{id}"

  if steps.indexOf(payload.step)+2 is steps.length
    logger.info "Application #{req.params.id} is at the last step. Submitting..."
    Request.put {url: "#{req.brand.apiUrl}/account-applications/#{req.params.id}/submit?lang=#{req.language}&brand=#{req.brand.id}", gzip: true}, (err, response, body) ->
      renderedError = renderError(err, response, body, req, res)
      if renderedError then return else res.redirect url
  else
    res.redirect url


routeHandler = (req, res) ->
  if req.params.id
    logger.info "Updating application #{req.params.id}"
    req.pipe Request.put {url: "#{req.brand.apiUrl}/account-applications/#{req.params.id}", gzip: true}, (err, response, body) ->
      handleResponse err, response, body, req, res
  else
    logger.info "Creating new application"
    req.pipe Request.post {url: "#{req.brand.apiUrl}/account-applications", gzip: true}, (err, response, body) ->
      handleResponse err, response, body, req, res

fileHandler = (req, res) ->
  logger.info "Uploading a file for #{req.params.id}"

  # In Express 4+, req.files is no longer available on the req object by default
  # To access uploaded files on the req.files object, we use middleware Multiparty
  form = new multiparty.Form()
  form.parse req, (err, fields, files) ->
    # example what files variable look like:
    # proofOfIdentityBack[
    #   fieldName: 'proofOfIdentityBack',
    #   originalFilename: 'myPic.jpg',
    #   path: '/var/folders/tw/3wzkd94x3d3_3yq7_6hcp_wh0000gn/T/vNZtK9DZSXdvUWsGjAP3gj4d.jpg',
    #   headers: [Object],
    #   size: 215830
    # ]
    propertyName = Object.getOwnPropertyNames(files)
    filename = files[propertyName][0].fieldName # proofOfIdentityBack
    contentType = files[propertyName][0].headers['content-type'] # image/jpeg

    # First step, get the signed request url for uploading to S3
    Request.get {url: "#{req.brand.apiUrl}/account-applications/#{req.params.id}/sign-s3?filename=#{filename}&contentType=#{contentType}"}, (err, response, body) ->
      if err
        logger.error "File upload failed for #{req.params.id}"
        res.end(400)
      payload = JSON.parse(body)

      # Second step, upload file to S3 directly via obtained signed request url
      Request.put {url: payload.signedUrl, headers: {"Content-Type": contentType}, body: fs.readFileSync files[propertyName][0].path}, (err, response, body) ->
        if err
          logger.error "File upload failed for #{req.params.id}"
          res.end(400)

        # Last step, update account application with newly added file
        Request.put {url: "#{req.brand.apiUrl}/account-applications/#{req.params.id}", form: {"#{filename}": payload.location}, gzip: true}, (err, response, body) ->
          if err
            logger.error "File upload not completed for #{req.params.id}"
            res.end(400)
          res.status(200).json({result: 'done'})


router.post '/', routeHandler
router.post '/:id', routeHandler
router.post '/:id/file', fileHandler

module.exports = router
