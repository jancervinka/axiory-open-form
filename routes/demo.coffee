express = require 'express'
router = express.Router();
logger = require '../logger'
setApplicationDetails = require '../middleware/set-application-details'
Request = require 'request'
renderError = require('../utils').renderError

handler = (req, res) ->

	logger.info "Rendering demo"

	if (not req.brand)
		logger.error "No brand information!"
		res.render 'noBrandError'
		return true

	res.render 'demo',
		brand: req.brand

openHandler = (req, res) ->

	logger.info "Creating new demo account"
	req.pipe Request.post {url: "#{req.brand.apiUrl}/trading-accounts/demo", gzip: true}, (err, response, body) ->
		renderedError = renderError(err, response, body, req, res)
		if renderedError then return
		res.render 'demo-result',
			brand: req.brand
			account: if body then JSON.parse(body)

router.get '/', setApplicationDetails, handler
router.post '/open', setApplicationDetails, openHandler

module.exports = router
