express = require 'express'
router = express.Router();
setApplicationDetails = require '../middleware/set-application-details'
getStrategies = require '../middleware/get-strategies'
logger = require '../logger'

handler = (req, res) ->
  step = if req.step then req.step else 'intro'
  multitraders = if req.strategies then req.strategies else []

  logger.info "Rendering #{step}" + (if req.params.id then " for #{req.params.id}" else "")

  if (not req.brand)
  	logger.error "No brand information!"
  	res.render 'noBrandError'
  	return true

  res.render step,
    step: step
    brand: req.brand
    multitraders: multitraders
    id: req.params.id
    isPlain: if req.query?.plain is 'true' then true else false

router.get '/:id', setApplicationDetails, getStrategies, handler
router.get '/', setApplicationDetails, handler

module.exports = router
