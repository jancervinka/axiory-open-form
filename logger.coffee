winston = require 'winston'
require('winston-papertrail').Papertrail
port = require('config').get 'papertrail.port'

class Logger
  constructor: () ->
    winston.loggers.add 'logger',
      console:
        handleExceptions: true
        level: 'verbose'
        colorize: true
        timestamp: true
      papertrail:
        level: 'verbose'
        host: 'logs.papertrailapp.com'
        hostname: process.env.NODE_ENV + '-OpenForm'
        port: port
        colorize: false
        program: 'App'
        handleExceptions: true

    @logger = winston.loggers.get('logger')
    @logger.exitOnError = false

module.exports = new Logger().logger
