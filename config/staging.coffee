module.exports =
	defaultHostname: "axiory.eu"
	apiUrls:
		default: "http://api-staging.axiory.eu"
		global: "http://api-staging.axiory.com"
		europe: "http://api-staging.axiory.eu"
		pivot: "https://api-staging.pivot-markets.com"
