module.exports =
  httpsForwarding: true
  defaultHostname: "axiory.eu"
  apiUrls:
    default: "https://api.axiory.eu"
    global: "https://api.axiory.com"
    europe: "https://api.axiory.eu"
    pivot: "https://api.pivot-markets.com"
