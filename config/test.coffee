module.exports =
	defaultHostname: "axiory.eu"
	apiUrls:
	  default: "http://localhost:5000"
	  global: "http://localhost:5000"
	  europe: "http://localhost:5000"
	  pivot: "http://localhost:5000"
