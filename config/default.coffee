module.exports =
  httpsForwarding: false
  defaultHostname: "axiory.cz"
  apiUrls:
    default: "http://localhost:5000"
    global: "http://localhost:5000"
    europe: "http://localhost:5000"
    pivot: "http://localhost:5000"
  steps: ["intro", "personal-details", "investment-questionaire", "files", "trading-account-details", "done"]
  papertrail:
    port: 45279

