var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var nodemon = require('gulp-nodemon');
var browserSync = require('browser-sync');
var prefix = require('gulp-autoprefixer');
var reload = browserSync.reload;
var coffee = require('gulp-coffee');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

gulp.task('coffee', function() {
  return gulp.src(['./coffeescripts/lib/*.coffee', './coffeescripts/*.coffee'])
    .pipe(sourcemaps.init())
    .pipe(coffee({bare: true}))
    .pipe(concat('scripts.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./public/scripts/'));
});

gulp.task('brands', ['coffee'], function() {
  return gulp.src('./coffeescripts/brands/*.coffee')
    .pipe(sourcemaps.init())
    .pipe(coffee({bare: true}))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./public/scripts/'));
});

gulp.task('productionBrands', ['productionCoffee'], function() {
  return gulp.src('./coffeescripts/brands/*.coffee')
    .pipe(coffee({bare: true}))
    .pipe(uglify())
    .pipe(gulp.dest('./public/scripts/'));
});

gulp.task('sass', function () {
  gulp.src(['./sass/*.scss', './sass/*.sass'])
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(prefix())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./public/stylesheets'))
    .pipe(reload({stream:true}));
});

gulp.task('productionCoffee', function() {
  return gulp.src(['./coffeescripts/lib/*.coffee', './coffeescripts/*.coffee'])
    .pipe(coffee({bare: true}))
    .pipe(concat('scripts.js'))
    .pipe(gulp.dest('./public/scripts/'));
});

gulp.task('productionSass', function () {
  gulp.src(['./sass/*.scss', './sass/*.sass'])
    .pipe(sass())
    .pipe(prefix())
    .pipe(gulp.dest('./public/stylesheets'));
});

gulp.task('productionMergeJs', ['productionCoffee'], function() {
  gulp.src(['./public/scripts/lib/jquery.js', './public/scripts/lib/jqueryui.js', './public/scripts/lib/jquery.*.js', './public/scripts/lib/readmore.js', './public/scripts/lib/urlparam.js', './public/scripts/scripts.js'])
    .pipe(concat('all.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./public/scripts/'));
});

gulp.task('mergeJs', ['coffee'], function() {
  gulp.src(['./public/scripts/lib/jquery.js', './public/scripts/lib/jqueryui.js', './public/scripts/lib/jquery.*.js', './public/scripts/lib/readmore.js', './public/scripts/lib/urlparam.js', './public/scripts/scripts.js'])
    .pipe(sourcemaps.init())
    .pipe(concat('all.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./public/scripts/'));
});

gulp.task('browser-sync', ['server'], function() {
  browserSync({
    proxy: "localhost:4000"
  });
});

// Watch Files For Changes
gulp.task('watch', function() {
  gulp.watch(['sass/**/*.scss', 'sass/**/*.sass'], ['sass']);
  gulp.watch('./coffeescripts/**/*.coffee', ['coffee', 'mergeJs', 'brands', reload]);
  gulp.watch(["views/**/*.jade"], reload);
});

gulp.task('server', function () {
  nodemon({
    script: 'app.js',
    ext: 'js coffee json',
    ignore: ['public', 'coffeescripts'],
    env: { 'NODE_ENV': 'development' },
  })
    .on('restart', function () {
      console.log('restarted!');
      reload();
    });
});

// Default Task
gulp.task('build', ['sass', 'coffee', 'mergeJs', 'brands']);
gulp.task('productionBuild', ['productionSass', 'productionCoffee', 'productionMergeJs', 'productionBrands']);
gulp.task('default', ['build', 'watch', 'browser-sync']);
