# set variables for environment
express = require 'express'
app = express()
path = require 'path'
i18n = require 'i18next'
cookieParser = require 'cookie-parser'
morgan  = require 'morgan'
logger = require './logger'

forceSSL = require 'express-force-ssl'
httpsForwarding = require('config').get 'httpsForwarding'
if httpsForwarding is on then app.use forceSSL

# i18n
i18n.init
  ignoreRoutes: ['public/']
  supportedLngs: ['en', 'cs', 'ja']
  preload: ['en', 'cs', 'ja']
  detectLngQS: 'lng'
  cookieName: 'lng'
  fallbackLng: 'en'
  fallbackOnEmpty: false


app.use i18n.handle # use in routes. will autodetect lang based on cookie, queryparam or locale
i18n.registerAppHelper app # use in templates

# views as directory for all template files
app.set 'views', path.join(__dirname, 'views')
app.set 'view engine', 'jade'

# include latency
# app.use '/uploads', (req,res,next) -> setTimeout next, 5000

# instruct express to server up static assets
app.use express.static('public')
app.use cookieParser()
app.use require('./middleware/brand')

morgan.token 'brand', (req, res) -> return req.brand.id
app.use morgan(':remote-addr on :brand - :method :url :status - :res[content-length] B :response-time ms',
  stream:
    write: (message, encoding) ->
      logger.log 'info', message.replace(/\r?\n|\r/g,"")
)

app.use '/submit', require './routes/submit'
app.use '/demo', require './routes/demo'
app.use '/', require './routes/index'

module.exports = app
