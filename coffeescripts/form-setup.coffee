$ ->
  # Language change --------------
  $("#language").change ->

    $('form').slideUp 200
    $('#processing').slideDown 400

    lng = $(@).val()
    queryString = if window.location.search.indexOf("plain=true") > -1 then "?plain=true&lng=#{encodeURIComponent(lng)}" else "?lng=#{encodeURIComponent(lng)}"
    window.location.search = queryString

  # Form submit ------------------
  form = $ 'form'
  formMessages = $ '.submit-message'
  successMessage = $ '#success'
  errorMessage = $ '#error'
  processingMessage = $ '#processing'

  form.submit (ev) ->
    if window.location.search.indexOf("plain=true") > -1 then form.attr('action', "#{form.attr 'action'}?plain=true")
    formMessages.slideUp(200)
    if not $('#ib-code').val() then $('#ib-code').val $('#partnerCode').val()
    formIsValid = form.validate()

    if formIsValid
      form.slideUp 200
      processingMessage.slideDown 400
    else
      processingMessage.slideUp 400
      form.slideDown 400
      errorMessage.slideDown 400
      return ev.preventDefault()

    if form.find('.uploading').length > 0
      setTimeout( ->
        form.submit()
      , 1000)
      return ev.preventDefault()

  # Country picker ---------------
  $(".select-to-autocomplete").selectToAutocomplete()
  $(".select-to-autocomplete").parent().removeClass 'dropdown'
  $(".select-to-autocomplete").parent().addClass 'input'

  # Ib setter -------------------
  ibCode = if $.urlParam('ib') then $.urlParam('ib') else $.cookie('ib')

  if ibCode
    domain = window.location.host.split(':')[0]
    domains = domain.split('.')
    tld = domains[domains.length-1]
    mainDomain = domains[domains.length-2]
    cookieDomain = ".#{mainDomain}.#{tld}"
    $.cookie 'ib', ibCode,
      expires: 90,
      path: '/',
      domain: cookieDomain

    if $('#ibCode').length then $('#ibCode').val(ibCode)
    if ibCode then $('#ibCode').closest('p').hide()


  # Readmore ---------------------
  $('.help').readmore
    speed: 400
    lessLink: ''
    collapsedHeight: 60
    heightMargin: 20
    moreLink: '<button class="read-more" tabindex="-1"><i class="fa fa-chevron-down"></i></button>'
    embedCSS: false

  # Noscript ---------------------
  $('.no-script').hide(0)

  # Password strength meter ------
  $('#mt4Password').blur ->
    $('#password-strength').slideUp 400

  $('#mt4Password').keyup ->
    zxcvbnResult = zxcvbn? $(@).val()
    scorePercentage = (100/5) * (zxcvbnResult?.score+1)
    if scorePercentage then $('#password-strength').slideDown 400
    switch zxcvbnResult?.score
      when 0 then $('#password-strength').addClass('error').removeClass('success').removeClass('warn')
      when 1 then $('#password-strength').addClass('warn').removeClass('success').removeClass('error')
      when 2 then $('#password-strength').addClass('warn').removeClass('success').removeClass('error')
      when 3 then $('#password-strength').addClass('success').removeClass('error').removeClass('warn')
      when 4 then $('#password-strength').addClass('success').removeClass('error').removeClass('warn')

    $('#password-strength .meter').css 'width', "#{scorePercentage}%"

  # Conditional fields triggers --
  triggeringElements = $ '[data-trigger-details]'

  triggeringElements.each ->

    # Hide elements that should be triggered later
    triggeredElementId = $(@).data "triggerDetails"
    triggeredElement = $ "##{triggeredElementId}"
    # triggeredElement.slideUp()
    # triggeredElement.find('[required]').removeAttr('required')

    fieldName = $(@).attr 'name'

    # Option element has value stored on its parent and has no name
    isOption = $(@).prop('tagName').toLowerCase() is 'option'

    # This element will be watched for change and its value will decide if triggered element is shown
    fieldElement =
      unless isOption
      then $("[name=#{fieldName}]")
      else $(@).closest('select')

    # Find all values that should trigger
    triggeringValues = []
    $("[data-trigger-details=\"#{triggeredElementId}\"]").each ->
      triggeringValues.push $(@).val()

    fieldElement.change ->
      fieldValue = unless isOption then fieldElement.filter(':checked').val() else fieldElement.val()
      if fieldValue in triggeringValues
        triggeredElement.slideDown(400)
        triggeredElement.find('input, select').each ->
          if $(@).data('required') is 'required' then $(@).attr('required', 'required')
      else
        triggeredElement.slideUp(400)
        triggeredElement.find('[required]').data('required', 'required').removeAttr('required')

    fieldElement.change()

  # File upload ------------------
  progressbar = '<div class="progress-bar"><span class="meter"></span></div>'
  spinner = '<i class="spinner fa fa-circle-o-notch fa-spin"></i>'
  $('input[type="file"]').each ->
    $(@).addClass 'fileupload'
    $(@).fileupload
      url: $(@).closest('form').data('fileuploadurl')
      dataType: 'json'
      autoUpload: true
      replaceFileInput: false
      formData: []
      start: (e) ->
        $(@).addClass('uploading')
        $(@).clearFieldMarks()
        if $(@).closest('label').find('.progress-bar').length is 0
          $(@).closest('label').append progressbar
        else
          $(@).closest('label').find('.progress-bar').slideDown 200

        $(@).closest('label').find('.meter').css 'width', "0%"
      done: (e, data) ->
        $(@).clearFieldMarks().markValid()
        $(@).closest('label').find('.progress-bar').slideUp 200
        $(@).closest('.input').find('.spinner').fadeOut 200
        fileList = '<span class="file-list">'
        fileList = "#{fileList}#{file.name}, " for file in data.originalFiles
        fileList = fileList.slice(0, -2)
        fileList += '</span>'
        $(@).closest('.input').append fileList
        $(@).remove()
      fail: (e, data) ->
        $(@).removeClass('uploading')
        $(@).val('')
        $(@).clearFieldMarks()
        $(@).markInvalid 'failed'
        $(@).closest('label').find('.progress-bar').slideUp 200
        $(@).closest('.input').find('.spinner').fadeOut 200

      progress: (e, data) ->
        progress = parseInt data.loaded / data.total * 100, 10
        $(@).closest('label').find('.meter').css 'width', "#{progress}%"

        if progress is 100
          if $(@).closest('.input').find('.spinner').length is 0
            $(@).closest('.input').append spinner
          else
            $(@).closest('.input').find('.spinner').fadeIn 200

  # Validation -------------------
  # Should be last to avoid validation being triggered on load
  $('form').attr('novalidate', 'novalidate')
  $('select').each -> $(@).validateOn('change')
  $('input').not('[type=file], [type=radio], [type=checkbox]').each -> $(@).validateOn('blur')
  $('input').filter('[type=radio], [type=checkbox]').each -> $(@).validateOn('change')
