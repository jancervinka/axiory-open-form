do (jQuery) ->
  $ = jQuery
  $.fn.markInvalid = (type) ->
    fieldWrapper = if @.attr('type') is 'radio' then @.closest('fieldset') else @.closest('label')
    fieldWrapper.addClass 'invalid'

    preferredMessageExists = fieldWrapper.find(".#{type}").length > 0
    preferredMessage = fieldWrapper.find(".#{type}").first()
    genericMessage = fieldWrapper.find(".incorrect").first()
    errorMessage = if preferredMessageExists then preferredMessage else genericMessage
    errorMessage.slideDown 400
    return false

  $.fn.markValid = ->
    fieldWrapper = if @.attr('type') is 'radio' then @.closest('fieldset') else @.closest('label')
    fieldWrapper.addClass 'valid'
    return true

  $.fn.clearFieldMarks = ->
    fieldWrapper = if @.attr('type') is 'radio' then @.closest('fieldset') else @.closest('label')
    @.closest('.invalid').removeClass 'invalid'
    @.closest('.valid').removeClass 'valid'
    @.parentsUntil(fieldWrapper).siblings(".validation-message").slideUp 200

  $.fn.validateField = ->
    fieldValue = if @.attr('type') is 'radio' or @.attr('type') is 'checkbox' then $("input[name=#{@.attr('name')}]:checked").val() else @.val()
    if (not fieldValue or not fieldValue.length > 0) and @.attr('required')? then return @.markInvalid 'empty'
    #else if @.hasClass('uploading') then return @.markInvalid 'unfinished'
    else if @.attr('pattern')? and not @.val().match @.attr('pattern') then return @.markInvalid 'incorrect'
    else if @.attr('min')? and +fieldValue < @.attr('min') then return @.markInvalid 'low'
    else if @.attr('max')? and +fieldValue > @.attr('max') then return @.markInvalid 'high'
    else if @.attr('maxlength')? and fieldValue.length > @.attr('maxlength') then return @.markInvalid 'long'
    else if @.attr('type') is 'password' and @.attr('id').indexOf('Repeat') > -1
      passwordId = @.attr('id').replace('Repeat', '')
      if $("##{passwordId}").val() isnt @.val() then @.markInvalid 'incorrect'
      else @.markValid()
    else return @.markValid()

  $.fn.validate = ->
    isValid = true
    if @.is('form')
      @.find('select, input').not('.ui-autocomplete-input').each ->
        $(@).clearFieldMarks()
        if $(@).validateField() is false then isValid = false
        return true
    else
      isValid = @.validateField()

    return isValid

  $.fn.validateOn = (eventName)->
    $(@)[eventName] ->
      $(@).clearFieldMarks()
      $(@).validate()


